const fs = require('fs');
const Path = require('path');
const gulp = require('gulp');
const zip = require('gulp-zip');
const gulpif = require('gulp-if');
const replace = require('gulp-replace');

// config
// * path - path to folder to start from. Relative to where the script is running
// * output - path to where to put result, Relative to where the script is running
// * cleanup - if original folder should be removed [default=false]

module.exports = function(folders, config, next) {
    var path = config.path || '.';
    var outputPath = config.output || 'packages';
    var prefix = config.prefix || '';

    var sourcePath = Path.resolve(process.cwd(), path);
    var dirName = Path.basename(sourcePath);

    var startTime = Date.now();
    var now = new Date().toJSON().replace(/:/g, '-').replace(/\.[0-9]{3}Z/, '');
    var dest = Path.join(outputPath, dirName + '-package-' + now);
    var replacement = "";

    if (config.dest) {
        dest = Path.join(outputPath, config.dest);
    }

    if (config.shared) {
        replacement = config.shared.replace(/\//, '\/');
        replacement = replacement.replace('*', '[a-zA-z0-9]+\/');
    }

    // Loop through each slide folder
    // Get source of build
    // Replace storyboard in presentation.json

    gulp.task('package', function(done) {
        var endTime, diffTime;
        var counter = 0;
        var ln = folders.length;

        function package(folder) {
            var src = folder;
            var cleanSrc = src.split(Path.sep).join('/');
            var name = cleanSrc; // Default
            var fldr = cleanSrc;
            var barePath = "";
            // If folder name is 'dist' or we got full path
            // then zip name will be parent folder
            if (folder === 'dist') {
                name = dirName;
            }
            if (/\/\*\*\/\*$/.test(cleanSrc)) {
                fldr = cleanSrc.replace(/\/\*\*\/\*$/, '');
                barePath = fldr;
                // Let's figure out the zip name
                name = Path.basename(fldr);
                if (name === 'dist' || name === 'build') {
                    fldr = Path.dirname(fldr);
                    name = Path.basename(fldr);
                }
            }
            else {
                src = Path.join(sourcePath, folder, '/**/*');
            }
            if (config.shared) {
                src = [src, "!" + Path.join(barePath, config.shared + '{,/**/*}')];
            }
            gulp.src(src, {nodir: true})
                .pipe(gulpif(!!config.shared, replace(new RegExp(replacement, 'g'), '../shared/', {skipBinary: true})))
                .pipe(zip(prefix + name + '.zip'))
                .pipe(gulp.dest(dest))
                .on('end', function() {
                    counter += 1;
                    if (counter >= ln) {
                        done();
                        endTime = Date.now();
                        diffTime = (endTime - startTime) / 1000;
                        // console.log("Project exported to " + dest + " in " + diffTime + " seconds. Enjoy!");
                        next(null, dest);
                    }
                });
        }

        // If we got a single folder then we'll name it after the parent folder
        // e.g. if we package 'dist' in efficacy-slide, efficacy-slide.zip will be created
        if (typeof folders === "string") {
            ln = 1;
            package(folders);
        }
        else {
            folders.map(function(folder) {
                package(folder);
            });
        }



    });

    gulp.start.apply(gulp, ['package']);
};
