# Demo content

This content is for testing extraction of web content.

Here is [a link](http://anthillagency.com) and we should be able to catch it.

And here is [a link with a title](http://github.com "GitHub") and we should be able to catch it too.

And here is ![an image](../shared/img/test.png) and we should be able to catch it too.
