"use strict";

const test = require('tap').test;
const Path = require('path');
const fs = require('fs');

const packager = require('../index');

test("package demo", function(t) {
    packager('./test/demo/**/*', {}, function(err, result) {
        t.error(err);
        t.ok(result);
        t.end();
    });
});

test("package demo", function(t) {
    packager('./test/demo/dist/**/*', {output: "./test/exports", dest: "test"}, function(err, result) {
        t.error(err);
        t.ok(result);
        t.end();
    });
});

test("package demo with shared content", function(t) {
    packager('./test/demo/**/*', {shared: "shared-*", dest: "with-shared-content"}, function(err, result) {
        t.error(err);
        t.ok(result);
        t.end();
    });
});
