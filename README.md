# Anthill Packager

Package content as zip.

It's the content of the supplied folders that will be packaged, not the folder itself.

## API
```
var packager = require('ah-packager');
packager(folders, config, callback);
```

### param - folders
The folders to package.

They can be an array of simple strings, an array of full paths, or a string (single folder).

Full paths need to end in a glob pattern, i.e. ```/**/*```

#### Examples
```
var packager = require('ah-packager');

packager(['pageOne', 'pageTwo'], {}, function(err, dest) {});
// "dest" ~= packages/projectName-package-2017-05-14T11-01-21
// Zip files will be pageOne.zip and pageTwo.zip

packager(['User/demo/pageOne/build/**/*', 'User/demo/pageTwo/build/**/*'], {}, function(err, dest) {});
// "dest" ~= packages/projectName-package-2017-05-14T11-01-21
// Zip files will be pageOne.zip and pageTwo.zip

packager('pageOne', {}, function(err, dest) {});
// "dest" ~= packages/projectName-package-2017-05-14T11-01-21
// Zip file will be pageOne.zip

packager('dist', {}, function(err, dest) {});
// "dest" ~= packages/projectName-package-2017-05-14T11-01-21
// Zip file will be projectName.zip
```

### config
#### path
Where to start from. Defaults to '.' (current folder)

#### output
Where to put the result, relative to current folder. Defaults to './packages'

#### dest
Name of folder to put the packages in. Defaults to [projectName]-package-[timestamp]

#### shared
If a folder should be excluded because it's shared across packages, e.g. "node_modules/shared-content" -> "../shared"

## Dist folders
Distribution folders get special treatment. If a folder is named "dist" or full paths are used, the generated zip will
be named after the parent folder.

### Examples
"efficacy-slide/dist" as folders param will generate "efficacy-slide.zip" file in packages folder (default).

```["efficacy-slide/build/**/*", "safety-slide/build/**/*"]``` as folders param will generate "efficacy-slide.zip" and "safety-slide.zip" files in packages folder (default).
